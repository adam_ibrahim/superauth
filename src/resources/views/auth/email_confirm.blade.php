@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Congratulations!</div>

                <div class="panel-body">
                    @if (session('warning'))
                        <div class="alert alert-warning">
                            {{ session('warning') }}
                        </div>
                    @endif
                    <p>Your have Successfully registered! one step left, need to confirm your email</p>
                    <p>Please check your email {{ $user->email }}, and click the confirmation button</p>
                    <a href="{{route('email.confirm.resend', [$user->id, 'en'])}}" class="btn btn-primary">Resend Confirmation Email</a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
