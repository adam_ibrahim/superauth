<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->boolean('confirmed')->default(false)->after('password')->comment('0=email not confirmed, 1=email confirmed');
            $table->string('confirm_token')->nullable()->after('confirmed');
            $table->string('roles')->default(4)->after('confirm_token')->comment('user access Roles string or an array {0=Admin, 1=Moderator 2=Author 3=User_advanced 4=User}');
            $table->boolean('status')->default(true)->after('roles')->comment('0=Inactive, 1=Active');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn(array('confirmed', 'confirm_token', 'roles', 'status'));
        });
    }
}
