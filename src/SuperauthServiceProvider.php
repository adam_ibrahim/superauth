<?php

namespace Hostato\Superauth;

use Illuminate\Support\ServiceProvider;

class SuperauthServiceProvider extends ServiceProvider
{
    /**
     * Perform post-registration booting of services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadRoutesFrom(__DIR__.'/routes/web.php');
        $this->loadMigrationsFrom(__DIR__.'/database/migrations');
        $this->loadTranslationsFrom(__DIR__.'/resources/lang', 'superauth');
        $this->loadViewsFrom(__DIR__.'/resources/views', 'superauth');

        $this->publishes([
            __DIR__.'/resources/lang' => resource_path('lang/vendor/superauth'),
            __DIR__.'/resources/views' => resource_path('views/vendor/superauth'),
            __DIR__.'/database/migrations/' => database_path('migrations')
        ]);
    }

    /**
     * Register any package services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}