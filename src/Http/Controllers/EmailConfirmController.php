<?php

namespace Hostato\Superauth\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Hostato\Superauth\Models\User;

class EmailConfirmController extends Controller
{


    /**
     * Register user email confirm
     *
     * @param  string  $confirm_token
     * @param  string  $abbr // Session language sign
     * @return array
     */
    public function confirmEmail($confirm_token, $abbr)
    {
        User::where('confirm_token', $confirm_token)->firstOrFail()
            ->update([
                'confirm_token' => null,
                'confirmed'=> true,
            ]);
        return redirect()->route('home')
            ->with('success', trans('yourEmailHasBeenConfirmed'));
    }

    /**
     * Register user email confirm
     *
     * @param  string  $confirm_token
     * @param  string  $abbr // Session language sign
     * @return void
     */
    public function emailConfirm($id, $abbr)
    {
       $user = User::where('id', $id)->firstOrFail();

        return view('auth.email_confirm')
            ->with('user', $user)
            ->with('info', implode(' ', [trans('youHaveSuccessfullyRegistered'), trans('needToConfirmYourEmail')]));
    }

    /**
     * Register user email confirm
     *
     * @param  string  $confirm_token
     * @param  string  $abbr // Session language sign
     * @return void
     */
    public function resendConfirm($id, $abbr)
    {
            $user = User::where('id', $id)->firstOrFail();
            if ($user->confirmed > 0 ) {
                $loginlink = route('login', \App::getLocale());
                return redirect()
                ->back()
                ->with('success', implode(' ', [trans('yourEmailHasBeenConfirmed'), trans('youMayLogin')]))
                ->with('loginlink', $link);
            }

            $user->sendConfirmationEmail();
        return redirect()
                ->back()
                ->with('success', implode(' ', [trans('youHaveSuccessfullyRegistered'), trans('confirmationEmailHasBeenResent')]));
    }
}
