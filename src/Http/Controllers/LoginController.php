<?php

namespace Hostato\Superauth\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Hostato\Superauth\Models\User;
class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = 'home/en';
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }



    public function login(Request $request)
    {

        $user = User::where('email', $request->email)->first();

        if (!$user->isConfirmed()){
            return redirect()->back()->with('warning', implode(' ', [ trans('yourEmailIsNotConfirmed'), trans('needToConfirmYourEmail'), trans('beforeLogin') ]));
        }

        if ($user->isModerator()){
           $this->SetRedirectTo()->route('admin.dashboard', \App::getLocale());
        }

        $this->validateLogin($request);

        // If the class is using the ThrottlesLogins trait, we can automatically throttle
        // the login attempts for this application. We'll key this by the username and
        // the IP address of the client making these requests into this application.
        if ($this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);

            return $this->sendLockoutResponse($request);
        }

        if ($this->attemptLogin($request)) {
            return $this->sendLoginResponse($request);
        }


        // If the login attempt was unsuccessful we will increment the number of attempts
        // to login and redirect the user back to the login form. Of course, when this
        // user surpasses their maximum number of attempts they will get locked out.
        $this->incrementLoginAttempts($request);

        return $this->sendFailedLoginResponse($request);
    }

    protected function validateLogin(Request $request)
    {
        $this->validate($request, [
            $this->username() => 'required|string|email',
            'password' => 'required|string',
        ],[
            'required' => trans('thisFieldIsRequired'),
            'string' => trans('thisFieldHasToString'),
            'email' => 'invalidEmailAddress',

        ]);
    }



    /**
     * Set redirect path
     *
     * @param   string $redirect
     */
    protected function SetRedirectTo($redirect)
    {
        $this->redirectTo = $redirect;
    }


}
