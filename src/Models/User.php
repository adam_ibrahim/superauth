<?php

namespace Hostato\Superauth\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'confirm_token', 'roles',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    // User Roles
    const ROLE_ADMIN =          0;
    const ROLE_MODERATOR =      1;
    const ROLE_AUTHOR =         2;
    const ROLE_USER_ADVANCED =  3; // WITH Membership or paid user
    const ROLE_USER =           4;
    const ROLE_MODERATORS = [self::ROLE_ADMIN, self::ROLE_MODERATOR, self::ROLE_AUTHOR ];
}
