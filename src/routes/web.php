<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Authentication Routes...R
    Route::group(['prefix' => '{abbr}',  'middleware' => ['web', 'lang']], function()
    {
        Route::get('login', 'Hostato\Superauth\Http\Controllers\LoginController@showLoginForm')->name('login');
        Route::post('login', 'Hostato\Superauth\Http\Controllers\LoginController@login');
        Route::post('logout', 'Hostato\Superauth\Http\Controllers\LoginController@logout')->name('logout');

        // Registration Routes...
        Route::get('register', 'Hostato\Superauth\Http\Controllers\RegisterController@showRegistrationForm')->name('register');
        Route::post('register', 'Hostato\Superauth\Http\Controllers\RegisterController@register');
        Route::get('register/confirm_email', 'Hostato\Superauth\Http\Controllers\RegisterController@emailConfirmShow')->name('email.confirm');

        // Password Reset Routes...
        Route::get('password/request', 'Hostato\Superauth\Http\Controllers\ForgotPasswordController@showLinkRequestForm')->name('password.request');
        Route::post('password/email', 'Hostato\Superauth\Http\Controllers\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
        Route::get('password/reset/{token}', 'Hostato\Superauth\Http\Controllers\ResetPasswordController@showResetForm')->name('password.reset');
        Route::post('password/reset', 'Hostato\Superauth\Http\Controllers\ResetPasswordController@reset');
    });
